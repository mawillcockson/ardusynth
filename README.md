# ardusynth

## Description

This project is a personal project to make a noisemaker.

## Status

Currently, it is in a state of disarray from trying to develop a 
working application, without much attention paid towards making a nice 
project.

## Changelog/History

Since this is a git repository, a brief overview of development can be 
gleaned from running

    git log --oneline --graph --decorate --all

This doesn't help, though, when I don't type out detailed commit 
messages (and it doesn't display detailed commit messages), so a brief 
timeline of development history is provided below:

3. 2015-12-25T02:35+06: Worked on CapSense, adjusting the interface and 
                        thusly my code, to try to clean up how 
                        Capacitive Sensors are interacted with.

2. 2015-12-14T18:12+06: Lots of reordering, reorganization, and 
                        pruning of the drectory
1. 2015-12-14T16:40+06: This README.md was created
