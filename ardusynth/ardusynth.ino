/* Begin: Includes*/
// Mozzi
#include <MozziGuts.h>
#include <Oscil.h> // oscillator template
#include <mozzi_midi.h> // for mtof
#include <ADSR.h>
#include <LowPassFilter.h>
// #include <IntMap.h>
#include <tables/chum9_int8.h> // tables for oscillator(s)
#include <tables/phasor256_int8.h>
#include <tables/sin256_int8.h>
#include <tables/square_analogue256_int8.h> // My additions

// CapacitiveSensor
#include <CapacitiveSensor.h>

// Faster math
#include <FastDivision.h>
/* End: Includes */

/* Begin: Definitions and settings */
#define serial_on (true)
#define CONTROL_RATE 64 // Hz at which to run updateControl()
#define num_oscils (3)  // Number of oscillators to have available (also determines degree of polyphony)
#define timeout_cycles (100) // The maximum number of cycles all the capacitive sensors will count to while waiting for the second pin to change state
#define number_of_keys (5) // Defines the number of keys available to press to play notes (currently this shouldn't be set to a different number)
#define number_of_buttons (3) // The number of buttons (This is unlikely to change, as probably exactly three [mode, up, down] will always be needed)
// All of the capacitive button assignments
#define SEND_KEY1    (A4)
#define SEND_KEY2    (A3)
#define SEND_KEY3    (A2)
#define SEND_KEY4    (A1)
#define SEND_KEY5    (A0)
#define SEND_MODE    (8)
#define SEND_UP      (8)
#define SEND_DOWN    (8)
#define RECEIVE_KEY1 (2)
#define RECEIVE_KEY2 (3)
#define RECEIVE_KEY3 (4)
#define RECEIVE_KEY4 (5)
#define RECEIVE_KEY5 (6)
#define RECEIVE_MODE (7)
#define RECEIVE_UP   (11)
#define RECEIVE_DOWN (12)
// Serial (not exactly global variables, but useful definitions)
#define tab (Serial.print("\t"))
#define nl (Serial.print("\n"))
#define key_thresh (30) // Threshold value for temporary, simplistic key press testing
#define btn_thresh (10) // Threshold value for pressing buttons
#define cutoff_step (1) // The amount by which to raise or lower the cutoff when the ADSR envelope isn't conrolling it
#define number_of_modes (7) // The total number of modes that can be accessed with the mode button
/* End: Definitions and settings */

/* Begin: Includes, definitions, and declarations in question */
#define max_oscils (5)
/* End: Includes, definitions, and declarations in question */


/* Begin: Global variables */


struct OSCIL {
    // Oscil <PHASOR256_NUM_CELLS, AUDIO_RATE> oscil(PHAOR256_DATA); // Can't pass table as parameter when instantiating for some reason; get a weir error
    Oscil <PHASOR256_NUM_CELLS, AUDIO_RATE> oscil;
    uint8_t oscil_used_by = 0;
    ADSR <CONTROL_RATE, AUDIO_RATE> adsr;
};
uint8_t adsr_notes = false;
uint8_t adsr_out   = false;

OSCIL oscillators[num_oscils];
uint8_t num_oscils_avail = num_oscils;


struct Key {
    CapacitiveSensor cs_ob = CapacitiveSensor(0,0);
    bool key_was_pressed = false;
    bool key_is_pressed = false;
    bool key_has_oscillator = false;
    uint8_t midi_note = 60;
    int pitch = mtof(midi_note);
};

Key keys[number_of_keys];
uint8_t number_keys_pressed = 0;

struct Button {
    CapacitiveSensor cs_ob = CapacitiveSensor(0,0);
    bool btn_was_pressed = false;
    bool btn_is_pressed = false;
};

struct Button mode_btn;

enum Modes {
    VOLUME,
    OCTAVE,
    SCALE,
    INSTRUMENT,
    LPF_CUTOFF,
    LPF_STATE,
    SET_NOTE,
    ADSR_ASSIGNMENT, // Working on this one
    BEND_PITCH, // From here down haven't been implemented
    ATTACK_LEVEL,
    DECAY_LEVEL,
    ATTACK_TIME,
    DECAY_TIME,
    SUSTAIN_TIME,
    RELEASE_TIME
};

Modes mode_number = VOLUME;

struct Button up_btn;
struct Button down_btn;

struct LPF {
    LowPassFilter lpf;
    uint8_t on = 0;
};
LPF lp_filter;

uint8_t volume = 15;

uint8_t scale = 0;

const int8_t* tables[] = {PHASOR256_DATA, // Global variable for instrument/table mode
                          CHUM9_DATA,
                          SIN256_DATA,
                          SQUARE_ANALOGUE256_DATA};
      #define num_tables (4)
uint8_t table = 0; // Global variable for instrument/table mode

/* End: Global variables */

/* Begin: Useful functions and subroutines */

void set_tables(OSCIL* (oscils),const int8_t* (TABLE)) {
    for (uint8_t i = 0; i < num_oscils; ++i) {
        // (oscillators+i)->oscil.setTable(TABLE);
        (oscils+i)->oscil.setTable(TABLE);
    }
}

void set_notes(Key* (array_of_keys),uint8_t* notes) {
    for (uint8_t i = 0; i < number_of_keys; ++i) {
        array_of_keys[i].midi_note = notes[i];
    }
    
    // Serial
    // Serial.println(notes[0]);
}

void set_pitches(Key* (array_of_keys)) {
    for (uint8_t i = 0; i < number_of_keys; ++i) {
        array_of_keys[i].pitch = mtof((int)(array_of_keys[i].midi_note));
    }
}

void read_button_states(Key* (array_of_keys), Button** (array_of_buttons)) {
    
    for (uint8_t i = 0; i < number_of_keys; ++i) {
        array_of_keys[i].key_is_pressed =
            ((array_of_keys[i].cs_ob.capacitiveSensorRaw(1) > key_thresh) ? true : false); // Keys have individual send pins with 1MOhm resistors
    }
    
    for (uint8_t i = 0; i < number_of_buttons; ++i) {
        array_of_buttons[i]->btn_is_pressed = 
            ((array_of_buttons[i]->cs_ob.capacitiveSensorRaw(1) > btn_thresh) ? true : false);
    }
    
    // key1_is_pressed = true;
    // key2_is_pressed = false;
    // key3_is_pressed = false;
    // key4_is_pressed = false;
    // key5_is_pressed = false;
    // mode_is_pressed = false;
    // up_is_pressed = false;
    // down_is_pressed = false;
}

void perform_mode_actions(uint8_t button); // Prototype for compiling

void determine_button_actions(Button* (mode), Button* (up), Button* (down)) {
    if ((mode->btn_is_pressed == true) && (mode->btn_was_pressed  == false)) {
        mode->btn_was_pressed  = true;
        mode_number = Modes(uint8_t(mode_number)+1);
        if (mode_number > (number_of_modes - 1)) {
            mode_number = VOLUME;
        }
        
        // Serial
        #if (serial_on == true)
        Serial.print("mode: ");
        tab;
        switch (mode_number) {
            case VOLUME: Serial.println("volume"); break;
            case OCTAVE: Serial.println("octave"); break;
            case SCALE: Serial.println("scale"); break;
            case INSTRUMENT: Serial.println("instrument"); break;
            case LPF_CUTOFF: Serial.println("low-pass filter cutoff"); break;
            case LPF_STATE: Serial.println("low-pass filter state"); break;
            case SET_NOTE: Serial.println("set root midi note"); break;
        }
        #endif
    } else if ((mode->btn_is_pressed == false) && (mode->btn_was_pressed  == true)) {
        mode->btn_was_pressed  = false;
    }
    // The mode changing is handled in the previous if conditional
    // The up and down button pressing triggers a call to mode_action() with an input of up (RECEIVE_UP) or down (RECEIVE_DOWN) (this is so modes can be expanded in the future for more keys to be used in mode_actions)
    if ((up->btn_is_pressed == true) && (up->btn_was_pressed  == false)) {
        up->btn_was_pressed  = true;
        perform_mode_actions(RECEIVE_UP);
    } else if ((up->btn_is_pressed == false) && (up->btn_was_pressed  == true)) {
        up->btn_was_pressed  = false;
    }
    if ((down->btn_is_pressed == true) && (down->btn_was_pressed  == false)) {
        down->btn_was_pressed  = true;
        perform_mode_actions(RECEIVE_DOWN);
    } else if ((down->btn_is_pressed == false) && (down->btn_was_pressed  == true)) {
        down->btn_was_pressed  = false;
    }
}

void perform_mode_actions(uint8_t button) {
    switch (mode_number) {
    
    // VOLUME (Default)
    // OCTAVE
    // SCALE
    // INSTRUMENT
    // LPF_CUTOFF
    // LPF_STATE
    // ADSR_ASSIGNMENT
        
        case OCTAVE: {
            switch (button) {
                case RECEIVE_UP: {
                    bool note_range_exceeded = false;
                    for (uint8_t i = 0; i < number_of_keys; ++i) { // Test if shifting all notes up an octave will cause any one to exceed the highest note on a piano
                        if ((keys[i].midi_note + 12) > 108) {
                            note_range_exceeded = true;
                            break;
                        }
                    }
                    
                    if (note_range_exceeded == false) {
                        for (uint8_t i = 0; i < number_of_keys; ++i) {
                            keys[i].midi_note += 12;
                        }
                        set_pitches(keys);
                        
                        // Serial
                        #if (serial_on == true)
                        Serial.println("octave up");
                        #endif
                    }
                    break;
                }
                case RECEIVE_DOWN: {
                    bool note_range_exceeded = false;
                    for (uint8_t i = 0; i < number_of_keys; ++i) { // Test if shifting all notes down an octave will cause any one to drop below the lowest note on a piano
                        if ((keys[i].midi_note - 12) < 21) {
                            note_range_exceeded = true;
                            break;
                        }
                    }
                    if (note_range_exceeded == false) {
                        for (uint8_t i = 0; i < number_of_keys; ++i) {
                            keys[i].midi_note -= 12;
                        }
                        set_pitches(keys);
                        
                        // Serial
                        #if (serial_on == true)
                        Serial.println("octave down");
                        #endif
                    }
                    break;
                }
            }
            break;
        }
        case SCALE: {
            #define num_of_scales (4)
            uint8_t scales[num_of_scales][number_of_keys] =
                                         {{0,2,4,5,7}, // Major scale
                                          {0,2,3,5,7}, // Minor scale
                                          {0,4,7,12,16}, // Major chord
                                          {0,3,7,12,15}}; // Minor chord
            switch (button) {
                case RECEIVE_UP: {
                    scale += 1;
                    
                    if (scale > (num_of_scales - 1)) {
                        scale = 0;
                    }
                    break;
                }
                case RECEIVE_DOWN: {
                    scale -= 1;
                    
                    if (scale > (num_of_scales - 1)) { // If scale underflows from 0 to 255, assign it the largest scale number possible, to simulate shorter underflow/wrap-around
                        scale = (num_of_scales - 1);
                    }
                    break;
                }
            }
            
            for (uint8_t i = 0; i < number_of_keys; ++i) {
                keys[i].midi_note = keys[0].midi_note + scales[scale][i];
                
                // Serial
                // Serial.println(keys[i].midi_note);
            }
            set_pitches(keys);
            
            // Serial
            #if (serial_on == true)
            Serial.print("Scale: ");
            tab;
            switch (scale) {
                case 0: Serial.println("Major scale"); break;
                case 1: Serial.println("Minor scale"); break;
                case 2: Serial.println("Major chord"); break;
                case 3: Serial.println("Minor chord"); break;
            }
            #endif
            
            break;
        }
        case INSTRUMENT: {
            switch (button) {
                case RECEIVE_UP: {
                    ++table;
                    if (table > (num_tables - 1)) {
                        table = (num_tables - 1);
                    }
                    
                    break;
                }
                case RECEIVE_DOWN: {
                    --table;
                    if (table > (num_tables - 1)) {
                        table = 0;
                    }
                    
                    break;
                }
            }
            set_tables(oscillators,tables[table]);
            
            // Serial
            #if (serial_on == true)
            switch (table) {
                case 0: Serial.println("Phasor"); break;
                case 1: Serial.println("Chum"); break;
                case 2: Serial.println("Sine wave"); break;
                case 3: Serial.println("Square wave"); break;
            }
            #endif
            
            break;
        }
        case LPF_CUTOFF: {
            switch (button) {
                case RECEIVE_UP: {
                    lp_filter.lpf.cutoff_frequency += cutoff_step;
                    if (lp_filter.lpf.cutoff_frequency >= 190) { // If the value rolled over
                        lp_filter.lpf.cutoff_frequency = 189;
                    }
                    break;
                }
                case RECEIVE_DOWN: {
                    lp_filter.lpf.cutoff_frequency -= cutoff_step;
                    if (lp_filter.lpf.cutoff_frequency >= 190) { // If the value underflowed
                        lp_filter.lpf.cutoff_frequency = 0;
                    }
                    
                    break;
                }
            }
            
            // Serial
            #if (serial_on == true)
            Serial.print("cutoff: ");
            tab;
            Serial.println(lp_filter.lpf.cutoff_frequency);
            #endif
            
            break;
        }
        case LPF_STATE: {
            lp_filter.on = uint8_t(!bool(lp_filter.on)); // If there's only two states, regardless of whether up or down is pressed, toggle state
            
            // Serial
            #if (serial_on == true)
            Serial.print("lpf state: ");
            tab;
            Serial.println(((bool(lp_filter.on)) ? "on" : "off"));
            // Serial.println(lp_filter.on);
            #endif
            
            break;
        }
        case SET_NOTE: {
            switch (button) {
                case RECEIVE_UP: {
                    keys[0].midi_note += 1;
                    
                    break;
                }
                case RECEIVE_DOWN: {
                    keys[0].midi_note -= 1;
                    
                    break;
                }
            }
            set_pitches(keys);
            
            // Serial
            #if (serial_on == true)
            Serial.print("Root MIDI note");
            tab;
            Serial.println(keys[0].pitch);
            #endif
            
            break;
        }
        /* case ADSR_ASSIGNMENT: {
            
            if (btn == RECEIVE_UP) {
                adsr_assignment = ADSR_Assignment(uint8_t(adsr_assignment)+1);
                if (adsr_assignment > 1) {
                    adsr_assignment = ADSR_Assignment(0);
                }
            } else if (btn == RECEIVE_DOWN) {
                adsr_assignment = ADSR_Assignment(uint8_t(adsr_assignment)-1);
                if (adsr_assignment < 0) {
                    adsr_assignment = ADSR_Assignment(1);
                }
            }
            
            break;
        } */
        /* case BEND_PITCH: {
            
            break;
        }
        case ATTACK_LEVEL: {
            
            break;
        }
        case DECAY_LEVEL: {
            
            break;
        }
        case ATTACK_TIME: {
            
            break;
        }
        case DECAY_TIME: {
            
            break;
        }
        case SUSTAIN_TIME: {
            
            break;
        }
        case RELEASE_TIME: {
            
            break;
        } */
        default: { // Volume
            #define volume_step (1)
            #define max_volume (15)
            switch (button) {
                case RECEIVE_UP: {
                    // Basically, when the up button is pressed in volume mode, if the volume still has enough room to go up without overflowing, increment it by the predetermined step.
                    //   If not and it would overflow, then set it to the max volume.
                    if (volume < (max_volume - volume_step)) {
                        volume += volume_step;
                    } else {
                        volume = max_volume;
                    }
                    break;
                }
                case RECEIVE_DOWN: {
                    if (volume > volume_step) {
                        volume -= volume_step;
                    } else {
                        volume = 0;
                    }
                    break;
                }
            }
            break;
        }
    }
}

void set_oscillator_frequencies(OSCIL* (array_of_oscils),Key* (array_of_keys)) {
    #if (num_oscils >= number_of_keys)
        for (uint8_t i = 0; i < num_oscils; ++i) {
            array_of_oscils[i].oscil.setFreq(array_of_keys[i].pitch);
        }
    #endif
    #if ((num_oscils < number_of_keys) && (num_oscils > 1))
        // First test if an oscillator is being requested for a key (the key was just pressed), and if the pool of oscillators isn't zero
        for (uint8_t j = 0; j < number_of_keys; ++j) {
            if ((array_of_keys[j].key_is_pressed == true)   && 
                (array_of_keys[j].key_was_pressed == false) &&
                (num_oscils_avail > 0))
            {
            // Make sure that it's indicated the key is being pressed
                array_of_keys[j].key_was_pressed = true;
            // Then find a free oscillator to use, and assigned the pitch and indicate that it's now being used
                for (uint8_t i = 0; i < num_oscils; ++i) {
                    if (array_of_oscils[i].oscil_used_by == 0) {
                        array_of_oscils[i].oscil.setFreq(array_of_keys[j].pitch);
                        array_of_oscils[i].oscil_used_by = (j + 1);
                        array_of_keys[j].key_has_oscillator = true;
                        number_keys_pressed += 1;
                        num_oscils_avail -= 1;
                        break;
                    }
                }
                
                
                
                // Serial
                #if (serial_on == true)
                if (array_of_keys[j].key_has_oscillator == false) {
                    Serial.print("Key ");
                    Serial.print(j);
                    Serial.println(" doesn't have an oscillator, but there was one available");
                }
                #endif
            }
            /* If the key isn't being pressed anymore, but was being pressed AND had an oscillator,
             *   deactivate the oscillator and indicate the key is no longer being pressed
             */
            else if ((array_of_keys[j].key_is_pressed == false) &&
                     (array_of_keys[j].key_was_pressed == true) &&
                     (array_of_keys[j].key_has_oscillator == true))
            {
                array_of_keys[j].key_was_pressed = false;
                number_keys_pressed -= 1;
                for (uint8_t i = 0; i < num_oscils; ++i) {
                    if (array_of_oscils[i].oscil_used_by == (j + 1)) {
                        array_of_oscils[i].oscil_used_by = 0;
                        array_of_keys[j].key_has_oscillator = false;
                        num_oscils_avail += 1;
                        break;
                    }
                }
            }
        }
    #endif
    #if (num_oscils == 1)
        for (uint8_t i = 0; i < number_of_keys; ++i) {
            if (array_of_keys[i].key_is_pressed == true) {
                array_of_oscils[0].oscil.setFreq(array_of_keys[i].pitch);
                array_of_oscils[0].oscil_used_by = (i + 1);
                break;
            }
        }
    #endif
}

/* End: Useful functions and subroutines */

/* If there is a desire to add a toggle-able effect that does something at audio rate, here's the steps:
 * 
 *  1. Add the name of the effect to the Modes enum list
 *  2. Write a case for the new mode in the perform_mode_actions() function
 *       that sets any options or settings for the mode
 *  3. Increase the number_of_modes definition by one
 *  4. If the effect can play nice with other effects, write new functions
 *       in the audio mixing functions section that takes an int16_t and
 *       int8_t passed by reference, and returns void, for every combination
 *       of effects that could possibly be wanted
 *  5. Write a case for the new mode in the perform_mode_actions() function
 *       that toggles a flag---typed as a uint8_t---for that effect
 *  6. Add the new functions as references to the jump_table array in
 *       updateAudio(), ordering them so that when the flags are summed for
 *       the index, it leads to the right functions (truth table fun!)
 */
/* Begin: Functions for audio mixing and effects application */

void no_effects(int16_t (&sum_of_oscil_outs), int8_t (&out_audio)) {
    for (uint8_t i = 0; i < num_oscils; ++i) {
        if (oscillators[i].oscil_used_by > 0) {
            sum_of_oscil_outs += oscillators[i].oscil.next();
        }
    }
    
    switch (number_keys_pressed) {
        case 1: {
            out_audio = (sum_of_oscil_outs*volume) >> 4;
            break;
        }
        case 2: {
            out_audio = (sum_of_oscil_outs*volume) >> 5;
            break;
        }
        case 3: {
            int16_t val = sum_of_oscil_outs*volume;
            out_audio = ((val < 0) ? (0 - ((int16_t) divu3((unsigned int) (0 - val)))) : ((int16_t) divu3((unsigned int) val))) >> 4;
            break;
        }
        case 4: {
            out_audio = (sum_of_oscil_outs*volume) >> 6;
            break;
        }
        default: {
            out_audio = 0;
            break;
        }
    }
}

void lpf_on_total(int16_t (&sum_of_oscil_outs), int8_t (&out_audio)) {
    for (uint8_t i = 0; i < num_oscils; ++i) {
        if (oscillators[i].oscil_used_by > 0) {
            sum_of_oscil_outs += oscillators[i].oscil.next();
        }
    }
    
    switch (number_keys_pressed) {
        case 1: {
            out_audio = lp_filter.lpf.next((sum_of_oscil_outs*volume) >> 4);
            break;
        }
        case 2: {
            out_audio = lp_filter.lpf.next((sum_of_oscil_outs*volume) >> 5);
            break;
        }
        case 3: {
            int16_t val = sum_of_oscil_outs*volume;
            out_audio = lp_filter.lpf.next(((val < 0) ? (0 - ((int16_t) divu3((unsigned int) (0 - val)))) : ((int16_t) divu3((unsigned int) val))) >> 4);
            break;
        }
        case 4: {
            out_audio = lp_filter.lpf.next((sum_of_oscil_outs*volume) >> 6);
            break;
        }
        default: {
            out_audio = 0;
            break;
        }
    }
}
/* End: Functions for audio mixing and effects application */

/* Begin: Mozzi's two important routines */

void updateControl() {
    Button* btns[] = {&mode_btn,&up_btn,&down_btn}; // Can this be declared as a const?
    read_button_states(keys,btns);
    determine_button_actions(&mode_btn,&up_btn,&down_btn);
    set_oscillator_frequencies(oscillators,keys);
}

int updateAudio() {
    int16_t total_audio = 0;
    int8_t audio_out;
    
    // Declaring a jump table of functions that take the same types of arguments and return the same type (void)
    void (*jump_table[])(int16_t (&sum_of_oscil_outs), int8_t (&out_audio)) = 
                                    {
                                        &no_effects,
                                        &lpf_on_total
                                    };
    // Calling the appropriate function based on one effect flag
    (*jump_table[lp_filter.on])(total_audio,audio_out);
    
    // Return the final, processed value
    return audio_out;
}

/* End: Mozzi's two important routines */

void setup() {
    // Serial
    #if (serial_on == true)
    Serial.begin(9600);
    delay(10);
    #endif
    
    keys[0].cs_ob  = CapacitiveSensor(SEND_KEY1,RECEIVE_KEY1); // Send pin is 8, receive pin is 2
    keys[1].cs_ob  = CapacitiveSensor(SEND_KEY2,RECEIVE_KEY2); // Send pin is 8, receive pin is 3
    keys[2].cs_ob  = CapacitiveSensor(SEND_KEY3,RECEIVE_KEY3); // Send pin is 8, receive pin is 4
    keys[3].cs_ob  = CapacitiveSensor(SEND_KEY4,RECEIVE_KEY4); // Send pin is 8, receive pin is 5
    keys[4].cs_ob  = CapacitiveSensor(SEND_KEY5,RECEIVE_KEY5); // Send pin is 8, receive pin is 6
    mode_btn.cs_ob = CapacitiveSensor(SEND_MODE,RECEIVE_MODE); // Send pin is 8, receive pin is 7
    up_btn.cs_ob   = CapacitiveSensor(SEND_UP,  RECEIVE_UP  ); // Send pin is 8, receive pin is 11
    down_btn.cs_ob = CapacitiveSensor(SEND_DOWN,RECEIVE_DOWN); // Send pin is 8, receive pin is 12
    
    for (uint8_t i = 0; i < number_of_keys; ++i) {
        keys[i].cs_ob.set_CS_Timeout_Millis(timeout_cycles);
        keys[i].cs_ob.set_CS_AutocaL_Millis(0xFFFFFFFF);
    }
    mode_btn.cs_ob.set_CS_Timeout_Millis(timeout_cycles);
    mode_btn.cs_ob.set_CS_AutocaL_Millis(0xFFFFFFFF);
    up_btn.cs_ob.set_CS_Timeout_Millis(timeout_cycles);
    up_btn.cs_ob.set_CS_AutocaL_Millis(0xFFFFFFFF);
    down_btn.cs_ob.set_CS_Timeout_Millis(timeout_cycles);
    down_btn.cs_ob.set_CS_AutocaL_Millis(0xFFFFFFFF);
    
    uint8_t default_note_vals[] = {60,62,64,65,67};
    set_notes(keys,default_note_vals);
    set_pitches(keys);
    
    set_tables(oscillators,PHASOR256_DATA);
    
    lp_filter.lpf.cutoff_frequency = 90;
    lp_filter.lpf.resonance = 200;
    
    startMozzi(CONTROL_RATE);
}

void loop() {
    audioHook();
}
