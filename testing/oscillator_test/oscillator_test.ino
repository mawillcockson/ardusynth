//#include <ADC.h>  // Teensy 3.1 uncomment this line and install http://github.com/pedvide/ADC
#include <MozziGuts.h>
#include <Oscil.h> // oscillator template
#include <tables/sin2048_int8.h> // sine table for oscillator

// use: Oscil <table_size, update_rate> oscilName (wavetable), look in .h file of table #included above
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin(SIN2048_DATA);


void setup(){
  Serial.begin(115200);

  delay(100);
  
  startMozzi(64); // up control rate for smooth frequency shifting
  aSin.setFreq(440.0f); // set the frequency
}

float largest = 0;
float smallest = 0;
void updateControl(){
  float val = aSin.next();
  if (val > largest) {
    largest = val;
  }
  if (val < smallest) {
    smallest = val;
  }
  Serial.print(smallest);
  Serial.print("\t");
  Serial.println(largest);
}


int updateAudio(){
  return 0; // Do nothing with audio
}


void loop(){
  audioHook(); // required here
}



