void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600,SERIAL_8N1);
  float val = (((float) 200)/((float) 1140));
  Serial.println(val,DEC);
  Serial.println(val,BIN);
  long val2 = (((unsigned long)((200.0 / 1140.0)*100000)/1000)*1000);
  Serial.println(val2,DEC);
  Serial.println(val2,BIN);
//  #define shift (11)
//  Serial.println(((val2 >> shift) << shift),DEC);
//  Serial.println(((val2 >> shift) << shift),BIN);
  Serial.print(-2);
  Serial.print("\t");
  Serial.println(-2,BIN);
  Serial.print(2);
  Serial.print("\t");
  Serial.println(2,BIN);
  Serial.println((uint8_t)(-2));
  Serial.println((uint8_t)(-2),BIN);
  Serial.println((uint8_t)(2));
  #define dropNeg(a) (  (a < 0)?0:a  )
  Serial.println(dropNeg(-2));
  Serial.println(dropNeg(2));
}

void loop() {
  // put your main code here, to run repeatedly:

}
