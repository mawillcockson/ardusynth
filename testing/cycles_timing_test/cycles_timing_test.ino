void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600,SERIAL_8N1);
  delay(100);
  Serial.println("Setup");
}

void wait(unsigned long wait_time) {
  uint8_t wait_cycles = 0;
  while (wait_cycles < wait_time) {
    ++wait_cycles;
  }
}

unsigned long end_time;
unsigned long start_time;
void loop() {
  start_time = micros();
  wait(9);
  end_time = micros();

  Serial.println(end_time - start_time);
  
  
  delay(100);
}
