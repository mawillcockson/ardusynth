void setup() {
    Serial.begin(9800,SERIAL_8N1);
    delay(10);
}

#define Sp(a) (Serial.print(a))
#define tab   (Serial.print("\t"))
#define nl    (Serial.print("\n"))

void print_hello_on_1(int (*num)) {
    if (*num == 1) {
        Sp("hello");
        
        *num += 1;
    }
}

//void print_hello_on_2((int*) num) {
    //if (*num == 2) {
        //Sp("hello");
        //
        //*num += 1;
    //}
//}

void loop() {
    int num1 = 1;
    print_hello_on_1(num1);
    
    
    //(int*) num2 = 2;
    //print_hello_on_2(num2);
}
