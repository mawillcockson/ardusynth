//#include <ADC.h>  // Teensy 3.1 uncomment this line and install http://github.com/pedvide/ADC
#include <MozziGuts.h>
#include <Oscil.h> // oscillator template
#include <tables/sin2048_int8.h> // sine table for oscillator

// use: Oscil <table_size, update_rate> oscilName (wavetable), look in .h file of table #included above
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin(SIN2048_DATA);
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> bSin(SIN2048_DATA);

// control variable, use the smallest data size you can for anything used in audio
float frequency_multiplier = 1;
#define fundamental_frequency (440)

#define mapto(a) (((float)(a)*(2.0f/1023.0f)) - 1)


void setup(){
  startMozzi(); // start with default control rate of 64
  aSin.setFreq(fundamental_frequency); // set the frequency
}


void updateControl(){
  
}


int updateAudio(){
  return (aSin.next()* gain)>>8; // shift back to STANDARD audio range, like /256 but faster
}


void loop(){
  audioHook(); // required here
}



