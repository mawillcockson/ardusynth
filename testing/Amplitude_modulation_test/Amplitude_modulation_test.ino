//#include <ADC.h>  // Teensy 3.1 uncomment this line and install http://github.com/pedvide/ADC
#include <MozziGuts.h>
#include <Oscil.h> // oscillator template
#include <tables/sin2048_int8.h> // sine table for oscillator

// use: Oscil <table_size, update_rate> oscilName (wavetable), look in .h file of table #included above
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin(SIN2048_DATA);
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> bSin(SIN2048_DATA);

// control variable, use the smallest data size you can for anything used in audio
float frequency_multiplier = 1;
#define fundamental_frequency (440.0f)

#define mapto(a) (((float)(a)*(2.0f/1023.0f)) + 0.1f)
#define PIN      (A0)


void setup(){
  Serial.begin(115200);

  delay(100);
  
  startMozzi(CONTROL_RATE); // start with default control rate of 64
  adcDisconnectAllDigitalIns();
  aSin.setFreq(fundamental_frequency); // set the frequency
  bSin.setFreq(fundamental_frequency);
}


void updateControl(){
  frequency_multiplier = mapto(mozziAnalogRead(PIN));
  Serial.println(frequency_multiplier,5);
  bSin.setFreq(fundamental_frequency*frequency_multiplier);
}


int updateAudio(){
  return (aSin.next() + bSin.next()) >> 1; // shift back to STANDARD audio range, like /2 but faster
}


void loop(){
  audioHook(); // required here
}



