#include <math.h>

#define PIN (A0)

#define factor (1000.0f/((float) log(1024.0f)))

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600,SERIAL_8N1);
  pinMode(PIN,INPUT);
  analogWrite(PIN,LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  unsigned long knob = analogRead(PIN);
  float knob_adjust = (log(knob)*factor);
  Serial.print(knob);
  Serial.print("\t");
  Serial.println(knob_adjust);
  delay(100);
}
