#include <CapacitiveSensor.h>

#define LED 13
// #define delay_time1(a) ((uint8_t)(((float)(dropNeg(a - 170)))*(255/1140.0)))
// #define dropNeg(a)     ((a < 0)?0:a)
// #define timeout_cycles (500)

#define SEND_KEY1    (A4)
#define SEND_KEY2    (A3)
#define SEND_KEY3    (A2)
#define SEND_KEY4    (A1)
#define SEND_KEY5    (A0)
#define SEND_MODE    (8)
#define SEND_UP      (8)
#define SEND_DOWN    (8)
#define RECEIVE_KEY1 (2)
#define RECEIVE_KEY2 (3)
#define RECEIVE_KEY3 (4)
#define RECEIVE_KEY4 (5)
#define RECEIVE_KEY5 (6)
#define RECEIVE_MODE (7)
#define RECEIVE_UP   (11)
#define RECEIVE_DOWN (12)
CapacitiveSensor cs_key1 = CapacitiveSensor(SEND_KEY1,RECEIVE_KEY1); // Send pin is 8, receive pin is 2
CapacitiveSensor cs_key2 = CapacitiveSensor(SEND_KEY2,RECEIVE_KEY2); // Send pin is 8, receive pin is 3
CapacitiveSensor cs_key3 = CapacitiveSensor(SEND_KEY3,RECEIVE_KEY3); // Send pin is 8, receive pin is 4
CapacitiveSensor cs_key4 = CapacitiveSensor(SEND_KEY4,RECEIVE_KEY4); // Send pin is 8, receive pin is 5
CapacitiveSensor cs_key5 = CapacitiveSensor(SEND_KEY5,RECEIVE_KEY5); // Send pin is 8, receive pin is 6
CapacitiveSensor cs_mode = CapacitiveSensor(SEND_MODE,RECEIVE_MODE); // Send pin is 8, receive pin is 7
CapacitiveSensor cs_up = CapacitiveSensor(SEND_UP,RECEIVE_UP); // Send pin is 8, receive pin is 11
CapacitiveSensor cs_down = CapacitiveSensor(SEND_DOWN,RECEIVE_DOWN); // Send pin is 8, receive pin is 12
#define timeout_cycles (1000)

// void set_timeout_and_autocal_cycles(unsigned long timeout_cycles,CapacitiveSensor cs_ob) {
 // // cs_note1.set_CS_Timeout_Millis(timeout_cycles * (16000000.0f / ((float)310 * (float)F_CPU)));
    // cs_ob.CS_Timeout_Millis = timeout_cycles;
    // cs_ob.CS_AutocaL_Millis = 0xFFFFFFFF;
// }

void setup() {
    // Serial
    Serial.begin(9600,SERIAL_8N1);
    delay(100);

    // CapSense
    cs_key1.CS_Timeout_Millis = timeout_cycles;
    cs_key2.CS_Timeout_Millis = timeout_cycles;
    cs_key3.CS_Timeout_Millis = timeout_cycles;
    cs_key4.CS_Timeout_Millis = timeout_cycles;
    cs_key5.CS_Timeout_Millis = timeout_cycles;
    cs_mode.CS_Timeout_Millis = timeout_cycles;
    cs_up.CS_Timeout_Millis = timeout_cycles;
    cs_down.CS_Timeout_Millis = timeout_cycles;
    cs_key1.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_key2.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_key3.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_key4.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_key5.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_mode.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_up.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_down.CS_AutocaL_Millis = 0xFFFFFFFF;
    
    digitalWrite(LED,LOW);
    pinMode(LED,OUTPUT);
    digitalWrite(LED,LOW);
}

// Serial
long max_val = 0;
unsigned long max_time = 0;

// uint8_t delay_time2(unsigned long a) {
    // return ((uint8_t)((float) (dropNeg(a - 160) * (1000.0/1140.0))));
// }

unsigned long serial_time = millis();
void loop() {
//  digitalWrite(LED,LOW);
//  long first_val = cs_note1.capacitiveSensorRaw(1);
//  delay(100);
//  long second_val = cs_note1.capacitiveSensorRaw(1);
//  #define samps 1
//  long val = (cs_note1.capacitiveSensorRaw(samps) / samps);
    // Serial.println(cs_down.capacitiveSensorRaw(1));
    unsigned long val = cs_key1.capacitiveSensorRaw(1);
    unsigned long vals1 = cs_key2.capacitiveSensorRaw(1);
    unsigned long vals2 = cs_key3.capacitiveSensorRaw(1);
    unsigned long vals3 = cs_key4.capacitiveSensorRaw(1);
    unsigned long vals4 = cs_key5.capacitiveSensorRaw(1);
    unsigned long vals5 = cs_mode.capacitiveSensorRaw(1);
    unsigned long vals6 = cs_up.capacitiveSensorRaw(1);
    unsigned long vals7 = cs_down.capacitiveSensorRaw(1);
    
    Serial.print(val);
    Serial.print("\t");
    Serial.print(vals1);
    Serial.print("\t");
    // Serial.println(vals2);
    Serial.print(vals2);
    Serial.print("\t");
    Serial.print(vals3);
    Serial.print("\t");
    Serial.print(vals4);
    Serial.print("\t");
    Serial.print(vals5);
    Serial.print("\t");
    Serial.print(vals6);
    Serial.print("\t");
    Serial.println(vals7);
    
    delay(100);
    
    
    // #define thresh (30)
    // if ((val > thresh) || (vals1 > thresh) || (vals2 > thresh) || (vals3 > thresh) || (vals4 > thresh) || (vals5 > thresh) || (vals6 > thresh) || (vals7 > thresh)) {
        // digitalWrite(LED,HIGH);
    // } else {
        // digitalWrite(LED,LOW);
    // }
    
    
    
    
    // digitalWrite(LED,HIGH);
    // delay(map(val,0,timeout_cycles,0,1000));
    // digitalWrite(LED,LOW);
    
    // if ((millis() - serial_time) > 2000) { // Only print out measurements every 2 seconds
        // Serial
        // Serial.print(val);
        // Serial.print("\t");
        // Serial.print(max_val);
        // Serial.print("\t");
        // Serial.print(end_time - start_time);
        // Serial.print("\t");
        // Serial.println(max_time);
        
        // serial_time = millis();
    // }
    // if (val > max_val) {
        // max_val = val;
    // }
    // if ((end_time - start_time) > max_time) {
        // max_time = (end_time - start_time);
    // }
//  (fabs(second_val - first_val) > 15) ? digitalWrite(LED,HIGH) : digitalWrite(LED,LOW);
  // if the (floating point) absolute difference between the two readings is geater than 15, turn the LED on, else turn it off
//  digitalWrite(LED,HIGH);
//  delay(abs((val / 5000)*700));
//  digitalWrite(LED,LOW);
//  if (val >= 50) {
//    digitalWrite(LED,HIGH);
//    delay(500);
//    digitalWrite(LED,LOW);
//  }
  
//  Serial.print(val);
//  Serial.print("\t");
//  Serial.println(max_val);

    // delay(100);
}
