//#include <ADC.h>  // Teensy 3.1 uncomment this line and install http://github.com/pedvide/ADC
#include <MozziGuts.h>
#include <Oscil.h> // oscillator template
#include <tables/sin2048_int8.h> // sine table for oscillator

// use: Oscil <table_size, update_rate> oscilName (wavetable), look in .h file of table #included above
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin(SIN2048_DATA);
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> bSin(SIN2048_DATA);
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> cSin(SIN2048_DATA);
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> bass1(SIN2048_DATA);
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> bass2(SIN2048_DATA);

// control variable, use the smallest data size you can for anything used in audio
//float frequency_multiplier = 1;
//#define fundamental_frequency (440.0f)
float fundamental_frequency = 440;
#define harmony_frequency(a)     (((float)a)*(2.0f/5.0f))
#define bass1_frequency(a)       (((float)a)*(1.0f/4.0f))
#define bass2_frequency(a)       (((float)a)*(1.0f/5.0f))

#define mapInput(a) (  ((float)(a)) * (2.0f/1023.0f) + 6.0f  )
#define mapFreq(a) ((int)(map(a,-128,127,220,880)))
#define PIN      (A0)


void setup(){
  Serial.begin(115200,SERIAL_8N1);

  delay(100);
  
  startMozzi(512); // up control rate for smooth frequency shifting
  adcDisconnectAllDigitalIns();
  aSin.setFreq(fundamental_frequency); // set the frequency
  bSin.setFreq(fundamental_frequency);
  cSin.setFreq(fundamental_frequency);
  bass1.setFreq(fundamental_frequency);
  bass1.setFreq(fundamental_frequency);
}


void updateControl(){
//  frequency_multiplier = mapInput(mozziAnalogRead(PIN));
//  frequency_multiplier = 6.65494;
  fundamental_frequency = map(mozziAnalogRead(PIN),0,1023,220,5280);
//  Serial.println(frequency_multiplier,5);
//  Serial.println(fundamental_frequency,5);
//  bSin.setFreq(fundamental_frequency*frequency_multiplier);
  bSin.setFreq(fundamental_frequency);
  int val = mapFreq(bSin.next());
//  Serial.print(fundamental_frequency,5);
//  Serial.print("\t");
//  Serial.println(val);
  aSin.setFreq(val); // Very quickly set the frequency of the aSin oscillator using bSin as a second oscillator
  cSin.setFreq(harmony_frequency(val));
  bass1.setFreq(bass1_frequency(val));
  bass2.setFreq(bass2_frequency(val));
}


int updateAudio(){
//  aSin.setFreq(mapFreq(bSin.next())); // Very quickly set the frequency of the aSin oscillator using bSin as a second oscillator
//  return (aSin.next() + cSin.next() + bass1.next() + bass2.next()) >> 2; // shift back to STANDARD audio range, like /4 but faster
  return aSin.next();
}


void loop(){
  audioHook(); // required here
}



