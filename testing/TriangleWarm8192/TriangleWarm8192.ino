/*  Example playing a sinewave at a set frequency,
    using Mozzi sonification library.
  
    Demonstrates the use of Oscil to play a wavetable.
  
    Circuit: Audio output on digital pin 9 on a Uno or similar, or
    DAC/A14 on Teensy 3.1, or 
    check the README or http://sensorium.github.com/Mozzi/
  
    Mozzi help/discussion/announcements:
    https://groups.google.com/forum/#!forum/mozzi-users
  
    Tim Barrass 2012, CC by-nc-sa.
*/

//#include <ADC.h>  // Teensy 3.1 uncomment this line and install http://github.com/pedvide/ADC
#include <MozziGuts.h>
#include <Oscil.h> // oscillator template
#include <mozzi_midi.h>
#include <tables/triangle_warm8192_int8.h> // sine table for oscillator

// use: Oscil <table_size, update_rate> oscilName (wavetable), look in .h file of table #included above
Oscil <TRIANGLE_WARM8192_NUM_CELLS, AUDIO_RATE> aTriWarm(TRIANGLE_WARM8192_DATA);

// use #define for CONTROL_RATE, not a constant
#define CONTROL_RATE 64 // powers of 2 please


void setup(){
    startMozzi(CONTROL_RATE); // set a control rate of 64 (powers of 2 please)
    #define key1_pitch (mtof((int)60))
    #define key2_pitch (mtof((int)62))
    #define key3_pitch (mtof((int)64))
    #define key4_pitch (mtof((int)65))
    #define key5_pitch (mtof((int)67))
    aTriWarm.setFreq(key2_pitch); // set the frequency
}


void updateControl(){
    // aTriWarm.setFreq(mozziAnalogRead(A0));
    // put changing controls in here
}


int updateAudio(){
    return aTriWarm.next(); // return an int signal centred around 0
}


void loop(){
    audioHook(); // required here
}