#include <CapacitiveSensor.h>
#include <MozziGuts.h>
#define CONTROL_RATE 64

CapacitiveSensor cs_btn = CapacitiveSensor(8,6);
#define timeout_cycles (100)
//Setup
long previous = cs_btn.capacitiveSensorRaw(1);
long next     = previous;


void setup() {
    // Serial
    Serial.begin(9600);
    delay(100);
    
    cs_btn.CS_Timeout_Millis = timeout_cycles;
    cs_btn.CS_AutocaL_Millis = 0xFFFFFFFF;
    
    startMozzi(CONTROL_RATE);
}

void updateControl() {
    next = cs_btn.capacitiveSensorRaw(1);
    Serial.print(next);
    Serial.print("\t");
    Serial.println(next - previous);
    previous = next;
}

int updateAudio() {
    return 0;
}

void loop() {
    audioHook();
}

