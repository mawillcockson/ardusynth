#include <MozziGuts.h>
#include <Oscil.h>
#include <tables/sin2048_int8.h>
#include <math.h>

// use: Oscil <table_size, update_rate> oscilName (wavetable), look in .h file of table #included above
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin(SIN2048_DATA);
Oscil <2048, CONTROL_RATE> kWaver(SIN2048_DATA);

float center_freq = 440;
float depth = 0.5;
//byte gain = 255;


// use #define for CONTROL_RATE, not a constant
#define CONTROL_RATE 64 // powers of 2 please
#define PIN (A0)
#define waver_freq (10.0f)

void setup(){
  Serial.begin(9600);

  delay(100);
  pinMode(PIN,INPUT);
  analogWrite(PIN,LOW);
  startMozzi(CONTROL_RATE); // set a control rate of 64 (powers of 2 please)
  disconnectDigitalIn(PIN);
//  adcDisconnectAllDigitalIns();
  aSin.setFreq(center_freq); // set the frequency
  kWaver.setFreq(waver_freq);
}

#define midpoint (50)
#define factor (waver_freq/((float) log(1024.0f)))
void updateControl(){
  float knob = mozziAnalogRead(PIN);
//  float freq = center_freq - midpoint + knob;
  Serial.println(knob);
  kWaver.setFreq((float) log(knob)*factor);
  float waver = depth * kWaver.next();
  aSin.setFreq(center_freq + waver);
}


int updateAudio(){
//  return (aSin.next()*gain)>>8; // return an int signal centred around 0
  return aSin.next();
}


void loop(){
  audioHook(); // required here
}



