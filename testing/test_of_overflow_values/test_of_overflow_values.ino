void setup() {
    Serial.begin(9600);
    delay(10);
    
    #define tab (Serial.print("\t"))
    
    uint16_t val = (0U - 1);
    Serial.print("uint16_t underflow: ");
    tab;
    Serial.println(val);
    
    unsigned int val3 = (-1);
    Serial.print("unsigned int underflow: ");
    tab;
    Serial.println(val3);
    
    int16_t val2 = ((int16_t) val);
    Serial.print("uint16_t to int16_t: ");
    tab;
    Serial.println(val2);
}

void loop() {}