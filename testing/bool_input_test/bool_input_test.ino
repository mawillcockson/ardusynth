#include <CapacitiveSensor.h>

CapacitiveSensor cs_down = CapacitiveSensor(8,12);
#define timeout_cycles (100)

void setup() {
    Serial.begin(9600,SERIAL_8N1);
    delay(100);
    
    cs_down.CS_Timeout_Millis = timeout_cycles;
    cs_down.CS_AutocaL_Millis = 0xFFFFFFFF;
}

bool btn_states[8];
#define thresh (30)
void loop() {
    unsigned long val = cs_down.capacitiveSensorRaw(1);
    bool bool_val = ((val > thresh) ? true : false);
    // btn_states[8] = val;
    // Serial.print(btn_states[8]);
    // Serial.print("\t");
    Serial.print(bool_val);
    Serial.print("\t");
    Serial.println(val);
    // Serial.print("\t");
    // if (btn_states[8] == true) {
        // Serial.println("Button is pressed");
    // } else {
        // Serial.println("Button is not pressed");
    // }
    
    delay(100);
}