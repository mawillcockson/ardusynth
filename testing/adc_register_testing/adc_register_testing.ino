// Serial
#define Sp(a)  (Serial.print(a))
#define Spn(a) (Serial.println(a))
#define tab    (Sp("\t"))
#define nl     (Sp("\n"))

#define PIN (A0)

void setup() {
    // Serial
    Serial.begin(9600);
    //while(!Serial.ready){continue;}
    delay(10);
    
    Sp("ADSC:");
    tab;
    Serial.print(ADSC,BIN);
    nl;
    Sp("ADCSRA:");
    tab;
    Serial.println(ADCSRA,BIN);
    
    Spn("Turning off ADC...");
    ADCSRA = 0;
    Spn("ADC off");
    
    Serial.println(ADCSRA,BIN);
    
    pinMode(PIN,INPUT);
    Spn(analogRead(PIN));
    Serial.println(ADCSRA,BIN);
}

void loop() {
    Spn(analogRead(PIN));
    delay(700);
    
}
