//#include <ADC.h>  // Teensy 3.1 uncomment this line and install http://github.com/pedvide/ADC
#include <MozziGuts.h>        // at the top of your sketch
#include <tables/sin2048_int8.h>
#include <Oscil.h>
#include <CapacitiveSensor.h>
#define CONTROL_RATE 64   // or some other power of 2

Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin(SIN2048_DATA);

#define SEND (8)
#define RECEIVE_NOTE1 (2)
#define RECEIVE_NOTE2 (3)
#define RECEIVE_NOTE3 (4)
#define RECEIVE_NOTE4 (5)
#define RECEIVE_NOTE5 (6)
#define RECEIVE_MODE (7)
#define RECEIVE_UP (11)
#define RECEIVE_DOWN (12)
CapacitiveSensor cs_note1 = CapacitiveSensor(SEND,RECEIVE_NOTE1); // Send pin is 8, receive pin is 6
CapacitiveSensor cs_note2 = CapacitiveSensor(SEND,RECEIVE_NOTE2); // Send pin is 8, receive pin is 6
CapacitiveSensor cs_note3 = CapacitiveSensor(SEND,RECEIVE_NOTE3); // Send pin is 8, receive pin is 6
CapacitiveSensor cs_note4 = CapacitiveSensor(SEND,RECEIVE_NOTE4); // Send pin is 8, receive pin is 6
CapacitiveSensor cs_note5 = CapacitiveSensor(SEND,RECEIVE_NOTE5); // Send pin is 8, receive pin is 6
CapacitiveSensor cs_mode = CapacitiveSensor(SEND,RECEIVE_MODE); // Send pin is 8, receive pin is 6
CapacitiveSensor cs_up = CapacitiveSensor(SEND,RECEIVE_UP); // Send pin is 8, receive pin is 6
CapacitiveSensor cs_down = CapacitiveSensor(SEND,RECEIVE_DOWN); // Send pin is 8, receive pin is 6
#define timeout_time (100)

void set_timeout_and_autocal_cycles(unsigned long timeout_cycles,CapacitiveSensor cs_ob) {
//  cs_ob.set_CS_Timeout_Millis(timeout_cycles * (16000000.0f / ((float)310 * (float)F_CPU)));
  cs_ob.CS_Timeout_Millis = timeout_cycles;
  cs_ob.CS_AutocaL_Millis = 0xFFFFFFFF;
}

byte gain = 0;
void setup() {
  // Serial
  Serial.begin(9600,SERIAL_8N1);
  delay(100);

  // CapSense
  set_timeout_and_autocal_cycles(timeout_time,cs_note1);
  set_timeout_and_autocal_cycles(timeout_time,cs_note2);
  set_timeout_and_autocal_cycles(timeout_time,cs_note3);
  set_timeout_and_autocal_cycles(timeout_time,cs_note4);
  set_timeout_and_autocal_cycles(timeout_time,cs_note5);
  set_timeout_and_autocal_cycles(timeout_time,cs_mode);
  set_timeout_and_autocal_cycles(timeout_time,cs_up);
  set_timeout_and_autocal_cycles(timeout_time,cs_down);

  Serial.println("Got to here");
  
  // Mozzi
  aSin.setFreq(440.0f);
  startMozzi(CONTROL_RATE);
}

//bool pressed = 0;
bool pressed = 1;
void updateControl() {
  // your control code
  long val = cs_note1.capacitiveSensorRaw(1);
  
  // Test processor capacity for audio generation and input capture
  // long vals[7];
  // vals[1] = cs_note2.capacitiveSensorRaw(1);
  // vals[2] = cs_note3.capacitiveSensorRaw(1);
  // vals[3] = cs_note4.capacitiveSensorRaw(1);
  // vals[4] = cs_note5.capacitiveSensorRaw(1);
  // vals[5] = cs_mode.capacitiveSensorRaw(1);
  // vals[6] = cs_up.capacitiveSensorRaw(1);
  // vals[7] = cs_down.capacitiveSensorRaw(1);
  long vals1 = cs_note2.capacitiveSensorRaw(1);
  // long vals2 = cs_note3.capacitiveSensorRaw(1);
  // long vals3 = cs_note4.capacitiveSensorRaw(1);
  // long vals4 = cs_note5.capacitiveSensorRaw(1);
  // long vals5 = cs_mode.capacitiveSensorRaw(1);
  // long vals6 = cs_up.capacitiveSensorRaw(1);
  // long vals7 = cs_down.capacitiveSensorRaw(1);
  
  
  val = (val < 0) ? 100 : val;
  gain = map(val,0,100,0,255);
//  if (val > 50 || val == (-2)) {
//    pressed = true;
//  } else {
//    pressed = false;
//  }
//  Serial.println(val);
  Serial.println(gain);
}

int updateAudio() {
  // your audio code which returns an int between -244 and 243
  // actually, a char is fine
  if (pressed == true) {
    return (aSin.next()*gain)>>8;
  } else if (pressed == false) {
    return 0;
  }
}

void loop() {
  audioHook(); // fills the audio buffer
}

