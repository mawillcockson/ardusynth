#include <CapacitiveSensor.h>

#define LED 13
// #define delay_time1(a) ((uint8_t)(((float)(dropNeg(a - 170)))*(255/1140.0)))
// #define dropNeg(a)     ((a < 0)?0:a)
// #define timeout_cycles (500)

#define SEND (8)
#define RECEIVE_NOTE1 (2)
#define RECEIVE_NOTE2 (3)
#define RECEIVE_NOTE3 (4)
#define RECEIVE_NOTE4 (5)
#define RECEIVE_NOTE5 (6)
#define RECEIVE_MODE (7)
#define RECEIVE_UP (11)
#define RECEIVE_DOWN (12)
CapacitiveSensor cs_note1 = CapacitiveSensor(SEND,RECEIVE_NOTE1); // Send pin is 8, receive pin is 6
CapacitiveSensor cs_note2 = CapacitiveSensor(SEND,RECEIVE_NOTE2); // Send pin is 8, receive pin is 6
CapacitiveSensor cs_note3 = CapacitiveSensor(SEND,RECEIVE_NOTE3); // Send pin is 8, receive pin is 6
CapacitiveSensor cs_note4 = CapacitiveSensor(SEND,RECEIVE_NOTE4); // Send pin is 8, receive pin is 6
CapacitiveSensor cs_note5 = CapacitiveSensor(SEND,RECEIVE_NOTE5); // Send pin is 8, receive pin is 6
CapacitiveSensor cs_mode = CapacitiveSensor(SEND,RECEIVE_MODE); // Send pin is 8, receive pin is 6
CapacitiveSensor cs_up = CapacitiveSensor(SEND,RECEIVE_UP); // Send pin is 8, receive pin is 6
CapacitiveSensor cs_down = CapacitiveSensor(SEND,RECEIVE_DOWN); // Send pin is 8, receive pin is 6
#define timeout_cycles (100)

// void set_timeout_and_autocal_cycles(unsigned long timeout_cycles,CapacitiveSensor cs_ob) {
 // // cs_note1.set_CS_Timeout_Millis(timeout_cycles * (16000000.0f / ((float)310 * (float)F_CPU)));
    // cs_ob.CS_Timeout_Millis = timeout_cycles;
    // cs_ob.CS_AutocaL_Millis = 0xFFFFFFFF;
// }

void setup() {
    // Serial
    Serial.begin(9600,SERIAL_8N1);
    delay(100);

    // CapSense
    cs_note1.CS_Timeout_Millis = timeout_cycles;
    cs_note2.CS_Timeout_Millis = timeout_cycles;
    cs_note3.CS_Timeout_Millis = timeout_cycles;
    cs_note4.CS_Timeout_Millis = timeout_cycles;
    cs_note5.CS_Timeout_Millis = timeout_cycles;
    cs_mode.CS_Timeout_Millis = timeout_cycles;
    cs_up.CS_Timeout_Millis = timeout_cycles;
    cs_down.CS_Timeout_Millis = timeout_cycles;
    cs_note1.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_note2.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_note3.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_note4.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_note5.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_mode.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_up.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_down.CS_AutocaL_Millis = 0xFFFFFFFF;
    
    digitalWrite(LED,LOW);
    pinMode(LED,OUTPUT);
    digitalWrite(LED,LOW);
}

// Serial
long max_val = 0;
unsigned long max_time = 0;

// uint8_t delay_time2(unsigned long a) {
    // return ((uint8_t)((float) (dropNeg(a - 160) * (1000.0/1140.0))));
// }

unsigned long serial_time = millis();
void loop() {
//  digitalWrite(LED,LOW);
//  long first_val = cs_note1.capacitiveSensorRaw(1);
//  delay(100);
//  long second_val = cs_note1.capacitiveSensorRaw(1);
//  #define samps 1
//  long val = (cs_note1.capacitiveSensorRaw(samps) / samps);

    unsigned long start_time = micros();
    long val = cs_note1.capacitiveSensorRaw(1);
    long vals1 = cs_note2.capacitiveSensorRaw(1);
    long vals2 = cs_note3.capacitiveSensorRaw(1);
    long vals3 = cs_note4.capacitiveSensorRaw(1);
    long vals4 = cs_note5.capacitiveSensorRaw(1);
    long vals5 = cs_mode.capacitiveSensorRaw(1);
    long vals6 = cs_up.capacitiveSensorRaw(1);
    long vals7 = cs_down.capacitiveSensorRaw(1);
    unsigned long end_time = micros();

    if (val == -2) {
        val = timeout_cycles; // Set to timeout if timeout error is returned
    }
    else if (val == -1) {
        val = 2000; // Signal in case something went really wrong
    }
    digitalWrite(LED,HIGH);
    delay(map(val,0,timeout_cycles,0,1000));
    digitalWrite(LED,LOW);
    
    if ((millis() - serial_time) > 2000) { // Only print out measurements every 2 seconds
        // Serial
        Serial.print(val);
        Serial.print("\t");
        Serial.print(max_val);
        Serial.print("\t");
        Serial.print(end_time - start_time);
        Serial.print("\t");
        Serial.println(max_time);
        
        serial_time = millis();
    }
    if (val > max_val) {
        max_val = val;
    }
    if ((end_time - start_time) > max_time) {
        max_time = (end_time - start_time);
    }
//  (fabs(second_val - first_val) > 15) ? digitalWrite(LED,HIGH) : digitalWrite(LED,LOW);
  // if the (floating point) absolute difference between the two readings is geater than 15, turn the LED on, else turn it off
//  digitalWrite(LED,HIGH);
//  delay(abs((val / 5000)*700));
//  digitalWrite(LED,LOW);
//  if (val >= 50) {
//    digitalWrite(LED,HIGH);
//    delay(500);
//    digitalWrite(LED,LOW);
//  }
  
//  Serial.print(val);
//  Serial.print("\t");
//  Serial.println(max_val);

    // delay(100);
}
