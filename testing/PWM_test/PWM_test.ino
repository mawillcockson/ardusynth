/*  Example of pulse width modulation,
    using Mozzi sonification library.
     
    Based Miller Puckette's j03.pulse.width.mod example 
    in the Pure Data documentation, subtracting 2 sawtooth 
    waves with slightly different tunings to produce a 
    varying phase difference.
  
    Demonstrates Phasor().
  
    Circuit: Audio output on digital pin 9 on a Uno or similar, or
    DAC/A14 on Teensy 3.1, or 
    check the README or http://sensorium.github.com/Mozzi/
  
    Mozzi help/discussion/announcements:
    https://groups.google.com/forum/#!forum/mozzi-users
  
    Tim Barrass 2012, CC by-nc-sa.
*/

//#include <ADC.h>  // Teensy 3.1 uncomment this line and install http://github.com/pedvide/ADC
#include <MozziGuts.h>
#include <Phasor.h>

#define CONTROL_RATE 64 // powers of 2 please

Phasor <AUDIO_RATE> aPhasor1;
Phasor <AUDIO_RATE> aPhasor2;

float fun_freq = 55.f;

void setup(){
  // Serial
 Serial.begin(9600,SERIAL_8N1);
 delay(100);
  
  aPhasor1.setFreq(fun_freq);
  // aPhasor2.setFreq(fun_freq+0.2f);
  aPhasor2.setFreq(fun_freq);
  startMozzi(CONTROL_RATE); // set a control rate of 64 (powers of 2 please)
  adcDisconnectAllDigitalIns();
}

#define READ_PIN (mozziAnalogRead(A0))
//#define mapto(a) (((float)a)*(4.0f/1023.0f) - 2.0f)
// #define mapto(a) (((float)a)*(8.0f/1023.0f))
#define mapto(a) (((float)(a))*(((float)(0UL - 1))/1023.0F))
// #define mapto(a) (((float)(a))*(((float)(0UL - 4000000000))/1023.0F))
#define read_freq (mapto(READ_PIN))
unsigned long phase_offset = 0;
// bool up_or_down = 1; // 1 means going up, 0 means going down

void updateControl(){
    phase_offset = mapto(READ_PIN);
    aPhasor1.set(0UL);
    aPhasor2.set(phase_offset);
    Serial.println(phase_offset);
    // aPhasor1.setFreq(fun_freq);
//  aPhasor2.setFreq(fun_freq+(((float)phase_offset)/200.0f));
    // aPhasor2.setFreq(fun_freq+read_freq);
//  if (up_or_down == true && phase_offset < 127) {
//    ++phase_offset; // This will roll around at 127 to -128 and back up again, causing the phase difference between the two to ramp as well
//  } else if (up_or_down == false && phase_offset > (-128)) {
//    --phase_offset;
//  } else {
//    // If neither of the previous statements is true, that mean the value has reached it's the end of one direction, and needs to change directions
//    //   Not paying attention to which side it's on and adjusting for that does cause the value to be doubled, but it's negligible
//    up_or_down = !up_or_down; // Toggles to other state, from up to down, or vice-versa
//  }

  // Serial
//  Serial.print(phase_offset);
//  Serial.print("\t");
//  Serial.println(((float)phase_offset)/200.0f);
}


int updateAudio(){
  char asig1 = (char)(aPhasor1.next()>>24);
  char asig2 = (char)(aPhasor2.next()>>24);
  return ((int)asig1-asig2)/2;
//  return ((int) asig1)/2;
}


void loop(){
  audioHook(); // required here
}







