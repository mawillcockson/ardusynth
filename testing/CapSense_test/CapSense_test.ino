#include <CapacitiveSensor.h>

CapacitiveSensor cs_ob = CapacitiveSensor(4,2);

void setup() {
  cs_ob.set_CS_AutocaL_Millis(0xFFFFFFFF);
  Serial.begin(9600,SERIAL_8N1);
}

void loop() {
  // put your main code here, to run repeatedly:
  unsigned long start = millis();
  long val = cs_ob.capacitiveSensorRaw(1);
  
  Serial.print(millis() - start);
  Serial.print("\t");
  Serial.println(val);

  delay(500);
}
