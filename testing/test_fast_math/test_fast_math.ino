#include <FastDivision.h>

void setup() {
    Serial.begin(9600);
    delay(10);
}

int16_t total_audio = 0;
uint8_t volume = 0;
unsigned long start_time;
unsigned long end_time;
unsigned long total_time_regular = 0;
unsigned long total_time_new = 0;
#define tab (Serial.print("\t"))
#define nl  (Serial.print("\n"))
void loop() {
    while (total_audio < 1220) {
        ++total_audio;
        ++volume;
        if (volume > 15) {
            volume = 0;
        }
        
        // Serial.println("total_audio, volume:");
        // Serial.print(total_audio);
        // tab;
        // Serial.println(volume);
        
        start_time = millis();
        int16_t regular_val = ((total_audio*volume)/3) >> 4;
        end_time = millis();
        
        total_time_regular += (end_time - start_time);
        // Serial.print("Time for old operation: ");
        // tab;
        // Serial.println(end_time - start_time);
        
        start_time = millis();
        int16_t val = total_audio*volume;
        int16_t result_val = ((val < 0) ? (0 - ((int16_t) divu3((unsigned int) (0 - val)))) : ((int16_t) divu3((unsigned int) val))) >> 4;
        end_time = millis();
        
        total_time_new += (end_time - start_time);
        
        if (regular_val != result_val) {
            Serial.print("Something went wrong in first loop, with total_audio and volume as:");
            nl;
            Serial.print(total_audio);
            tab;
            Serial.print(volume);
            nl;
            Serial.print(regular_val);
            tab;
            Serial.print(result_val);
            while (true) {continue;}
        }
        
        // Serial.print("Time for new operation: ");
        // tab;
        // Serial.println(end_time - start_time);
        
        // nl;
        
        // Serial.println("Result of old operation, result of new one:");
        // Serial.print(regular_val);
        // tab;
        // Serial.println(result_val);
        
        // delay(2000);
    }
    Serial.print("Total time for old operation: ");
    Serial.print(total_time_regular);
    nl;
    Serial.print("Total_time for new operation: ");
    Serial.print(total_time_new);
    nl;
    
    total_audio = 0;
    volume = 0;
    total_time_regular = 0;
    total_time_new = 0;
    
    while (total_audio > (-1220)) {
        --total_audio;
        ++volume;
        if (volume > 15) {
            volume = 0;
        }
        
        start_time = millis();
        int16_t regular_val = ((total_audio*volume)/3) >> 4;
        end_time = millis();
        
        total_time_regular += (end_time - start_time);
        
        start_time = millis();
        int16_t val = total_audio*volume;
        int16_t result_val = ((val < 0) ? (0 - ((int16_t) divu3((unsigned int) (0 - val)))) : ((int16_t) divu3((unsigned int) val))) >> 4;
        end_time = millis();
        
        total_time_new += (end_time - start_time);
        
        if (regular_val != result_val) {
            Serial.print("Something went wrong in second loop, with total_audio and volume as:");
            nl;
            Serial.print(total_audio);
            tab;
            Serial.print(volume);
            nl;
            Serial.print(regular_val);
            tab;
            Serial.print(result_val);
            while (true) {continue;}
        }
    }
    Serial.print("Total time for old operation: ");
    Serial.print(total_time_regular);
    nl;
    Serial.print("Total_time for new operation: ");
    Serial.print(total_time_new);
    nl;
    
    delay(2000);
}