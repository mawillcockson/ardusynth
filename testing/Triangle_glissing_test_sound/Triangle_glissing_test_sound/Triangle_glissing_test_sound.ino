//#include <ADC.h>  // Teensy 3.1 uncomment this line and install http://github.com/pedvide/ADC
#include <MozziGuts.h>        // at the top of your sketch
#include <Line.h> // for smooth transitions
#include <Oscil.h> // oscillator template
#include <tables/triangle512_int8.h> // triangle table for oscillator
#include <mozzi_midi.h>

// use: Oscil <table_size, update_rate> oscilName (wavetable), look in .h file of table #included above
Oscil <TRIANGLE512_NUM_CELLS, AUDIO_RATE> aTriangle(TRIANGLE512_DATA);
Line <unsigned int> glisser;

unsigned long audio_steps_per_gliss = AUDIO_RATE / 4;

bool going_up = true;

#define CONTROL_RATE (64)

void setup() {
  startMozzi(CONTROL_RATE);
}

void updateControl() {
  glisser.set(aTriangle.phaseIncFromFreq(200),
              aTriangle.phaseIncFromFreq(400),
              audio_steps_per_gliss);
}

int updateAudio() {
  aTriangle.setPhaseInc(glisser.next());
  return aTriangle.next();
}

void loop() {
  audioHook(); // fills the audio buffer
}

