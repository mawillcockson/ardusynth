#include <Oscil.h> // oscillator template
#include <mozzi_midi.h> // for mtof
// #include <tables/triangle_warm8192_int8.h> // table for oscillator(s)
#include <tables/phasor256_int8.h>
#define CONTROL_RATE 64

// CapacitiveSensor
#include <CapacitiveSensor.h>

// Faster math
#include <FastDivision.h>


// Mozzi
// use: Oscil <table_size, update_rate> oscilName (wavetable), look in .h file of table #included above
#define num_oscils (3)
uint8_t num_oscils_avail = num_oscils;
#define max_oscils (5)
#if (num_oscils >= 1)
// Oscil <TRIANGLE_WARM8192_NUM_CELLS, AUDIO_RATE> oscil1(TRIANGLE_WARM8192_DATA);
Oscil <PHASOR256_NUM_CELLS, AUDIO_RATE> oscil1(PHASOR256_DATA);
uint8_t oscil1_used_by = 0; // This indicates which key is using which oscillator; if the number is 0, the oscillator isn't currently being used
#endif
#if (num_oscils >= 2)
// Oscil <TRIANGLE_WARM8192_NUM_CELLS, AUDIO_RATE> oscil2(TRIANGLE_WARM8192_DATA);
Oscil <PHASOR256_NUM_CELLS, AUDIO_RATE> oscil2(PHASOR256_DATA);
uint8_t oscil2_used_by = 0;
#endif
#if (num_oscils >= 3)
// Oscil <TRIANGLE_WARM8192_NUM_CELLS, AUDIO_RATE> oscil3(TRIANGLE_WARM8192_DATA);
Oscil <PHASOR256_NUM_CELLS, AUDIO_RATE> oscil3(PHASOR256_DATA);
uint8_t oscil3_used_by = 0;
#endif
// Extra oscillators if there's enough CPU time for them
#if (num_oscils >= 4)
// Oscil <TRIANGLE_WARM8192_NUM_CELLS, AUDIO_RATE> oscil4(TRIANGLE_WARM8192_DATA);
Oscil <PHASOR256_NUM_CELLS, AUDIO_RATE> oscil4(PHASOR256_DATA);
uint8_t oscil4_used_by = 0;
#endif
#if (num_oscils >= 5)
// Oscil <TRIANGLE_WARM8192_NUM_CELLS, AUDIO_RATE> oscil5(TRIANGLE_WARM8192_DATA);
Oscil <PHASOR256_NUM_CELLS, AUDIO_RATE> oscil5(PHASOR256_DATA);
uint8_t oscil5_used_by = 0;
#endif

// CapacitiveSensor
#define SEND_KEY1    (A4)
#define SEND_KEY2    (A3)
#define SEND_KEY3    (A2)
#define SEND_KEY4    (A1)
#define SEND_KEY5    (A0)
#define SEND_MODE    (8)
#define SEND_UP      (8)
#define SEND_DOWN    (8)
#define RECEIVE_KEY1 (2)
#define RECEIVE_KEY2 (3)
#define RECEIVE_KEY3 (4)
#define RECEIVE_KEY4 (5)
#define RECEIVE_KEY5 (6)
#define RECEIVE_MODE (7)
#define RECEIVE_UP   (11)
#define RECEIVE_DOWN (12)
CapacitiveSensor cs_key1 = CapacitiveSensor(SEND_KEY1,RECEIVE_KEY1); // Send pin is 8, receive pin is 2
CapacitiveSensor cs_key2 = CapacitiveSensor(SEND_KEY2,RECEIVE_KEY2); // Send pin is 8, receive pin is 3
CapacitiveSensor cs_key3 = CapacitiveSensor(SEND_KEY3,RECEIVE_KEY3); // Send pin is 8, receive pin is 4
CapacitiveSensor cs_key4 = CapacitiveSensor(SEND_KEY4,RECEIVE_KEY4); // Send pin is 8, receive pin is 5
CapacitiveSensor cs_key5 = CapacitiveSensor(SEND_KEY5,RECEIVE_KEY5); // Send pin is 8, receive pin is 6
CapacitiveSensor cs_mode = CapacitiveSensor(SEND_MODE,RECEIVE_MODE); // Send pin is 8, receive pin is 7
CapacitiveSensor cs_up = CapacitiveSensor(SEND_UP,RECEIVE_UP); // Send pin is 8, receive pin is 11
CapacitiveSensor cs_down = CapacitiveSensor(SEND_DOWN,RECEIVE_DOWN); // Send pin is 8, receive pin is 12
#define timeout_cycles (100)
#define number_of_buttons (8)
#define number_of_keys (5)

// Serial
#define tab (Serial.print("\t"))
#define nl  (Serial.print("\n"))

// NOTE: Define tables for oscillator note frequencies for key presses; also decide on logic to determine oscillator assignment when there are fewer oscillators than keys being pressed
int fundamental_frequency = 440; // Use calculations using this to determine other note frequencies for the scale centered around this pitch, and have a mode adjust this number to move the pitch up and down
// uint8_t volume = (0U - 1); // Cause variable to underflow so volume is set to max at start of program
uint8_t volume = 15;



bool key1_was_pressed = false; // 0 (false) is not pressed, true (1) is pressed
bool key1_is_pressed = false;
bool key1_has_oscil = false;
bool key2_was_pressed = false;
bool key2_is_pressed = false;
bool key2_has_oscil = false;
bool key3_was_pressed = false;
bool key3_is_pressed = false;
bool key3_has_oscil = false;
bool key4_was_pressed = false;
bool key4_is_pressed = false;
bool key4_has_oscil = false;
bool key5_was_pressed = false;
bool key5_is_pressed = false;
bool key5_has_oscil = false;
bool mode_was_pressed  = false;
bool mode_is_pressed = false;
#define number_of_modes (5)
uint8_t mode_number = 0; // Initial mode is volume, then octace, and then scale
bool up_was_pressed  = false;
bool up_is_pressed = false;
bool down_was_pressed  = false;
bool down_is_pressed = false;
// #if (num_oscils < 5)
uint8_t number_keys_pressed = 0;
// #endif
bool btn_states[number_of_buttons];
void check_inputs(void) {
    // This is going to change a lot as input checking is worked out on the side, amongst other things like speed
    // Note: Eventual logic will be:
    //   If the button is being pressed, and checking its corresponding key value shows it hasn't been pressed previously,
    //     increment the number_keys_pressed by 1, and flip the corresponding key value to true.
    //   If the button is not pressed, and checking the correspondng key value shows it has been pressed, decrement
    //     number_keys_pressed and flip the corresponding key value to false.
    //   Check if number_keys_pressed is greater than 5, and if so, there's an error
    // As a convenient tool, the pin numbers are used to assign numbers to the pins in calls referring to them
    
    // if ((key1_is_pressed == true) && (key1_was_pressed == false)) { // If the key is being pressed and wasn't previously, it was pushed
        // key1_was_pressed = true;
        // number_keys_pressed += 1;
    // } else if ((key1_is_pressed == false) && (key1_was_pressed == true)) { // If the key was being pressed and isn't now, it was released
        // key1_was_pressed = false;
        // number_keys_pressed -= 1;
    // }
    // if ((key2_is_pressed == true) && (key2_was_pressed == false)) {
        // key2_was_pressed = true;
        // number_keys_pressed += 1;
    // } else if ((key2_is_pressed == false) && (key2_was_pressed == true)) {
        // key2_was_pressed = false;
        // number_keys_pressed -= 1;
    // }
    // if ((key3_is_pressed == true) && (key3_was_pressed == false)) {
        // key3_was_pressed = true;
        // number_keys_pressed += 1;
    // } else if ((key3_is_pressed == false) && (key3_was_pressed == true)) {
        // key3_was_pressed = false;
        // number_keys_pressed -= 1;
    // }
    // if ((key4_is_pressed == true) && (key4_was_pressed == false)) {
        // key4_was_pressed = true;
        // number_keys_pressed += 1;
    // } else if ((key4_is_pressed == false) && (key4_was_pressed == true)) {
        // key4_was_pressed = false;
        // number_keys_pressed -= 1;
    // }
    // if ((key5_is_pressed == true) && (key5_was_pressed == false)) {
        // key5_was_pressed = true;
        // number_keys_pressed += 1;
    // } else if ((key5_is_pressed == false) && (key5_was_pressed == true)) {
        // key5_was_pressed = false;
        // number_keys_pressed -= 1;
    // }
    if ((mode_is_pressed == true) && (mode_was_pressed  == false)) {
        mode_was_pressed  = true;
        mode_number += 1;
        if (mode_number > number_of_modes) {
            mode_number = 0;
        }
    } else if ((mode_is_pressed == false) && (mode_was_pressed  == true)) {
        mode_was_pressed  = false;
    }
    // The mode changing is handled in the previous if conditional
    // The up and down button pressing triggers a call to mode_action() with an input of up (RECEIVE_UP) or down (RECEIVE_DOWN) (this is so modes can be expanded in the future for more keys to be used in mode_actions)
    if ((up_is_pressed == true) && (up_was_pressed  == false)) {
        up_was_pressed  = true;
        mode_action(RECEIVE_UP);
    } else if ((up_is_pressed == false) && (up_was_pressed  == true)) {
        up_was_pressed  = false;
    }
    if ((down_is_pressed == true) && (down_was_pressed  == false)) {
        down_was_pressed  = true;
        mode_action(RECEIVE_DOWN);
    } else if ((down_is_pressed == false) && (down_was_pressed  == true)) {
        down_was_pressed  = false;
    }
}
// Checks if the given key is pressed or not
void check_button_states(void) {
    // This is going to change a lot as input checking is worked out on the side, amongst other things like speed
    #define thresh (30) // Threshold value for temporary, simplistic button press testing
    
    key1_is_pressed = ((cs_key1.capacitiveSensorRaw(1) > thresh) ? true : false);
    key1_is_pressed = true;
    
    key2_is_pressed = ((cs_key2.capacitiveSensorRaw(1) > thresh) ? true : false);
    key2_is_pressed = false;
    
    key3_is_pressed = ((cs_key3.capacitiveSensorRaw(1) > thresh) ? true : false);
    key3_is_pressed = true;
    
    key4_is_pressed = ((cs_key4.capacitiveSensorRaw(1) > thresh) ? true : false);
    key4_is_pressed = false;
    
    key5_is_pressed = ((cs_key5.capacitiveSensorRaw(1) > thresh) ? true : false);
    key5_is_pressed = false;
    
    mode_is_pressed = ((cs_mode.capacitiveSensorRaw(1) > thresh) ? true : false);
    mode_is_pressed = false;
    
    up_is_pressed = ((cs_up.capacitiveSensorRaw(1) > thresh) ? true : false);
    up_is_pressed = false;
    
    down_is_pressed = ((cs_down.capacitiveSensorRaw(1) > thresh) ? true : false);
    down_is_pressed = false;
}
// Changes an attribute of the program depending on if the up or down buttons is pressed and what the current mode is
void mode_action (uint8_t btn) {
    if (mode_number == 0) { // Volume
        #define volume_step (20)
        if (btn == RECEIVE_UP) {
            // Basically, when the up button is pressed in volume mode, if the volume still has enough room to go up without overflowing, increment it by the predetermined step.
            //   If not and it would overflow, then set it to the max volume.
            if (volume < (255 - volume_step)) {
                volume += volume_step;
            } else {
                volume = 255;
            }
        } else if (btn == RECEIVE_DOWN) {
            if (volume > volume_step) {
                volume -= volume_step;
            } else {
                volume = 0;
            }
        }
    }
    
    else if (false) { // Octave
        
    }
    
    else if (false) { // Scale
        
    }
    
    else if (false) { // Instrument
        
    }
    
    else if (false) { // Pitch bending
        
    }
}
void set_notes (void) {
    #define key1_pitch (mtof((int)60))
    #define key2_pitch (mtof((int)62))
    #define key3_pitch (mtof((int)64))
    #define key4_pitch (mtof((int)65))
    #define key5_pitch (mtof((int)67))
    
    #if (num_oscils >= number_of_keys)
        #if (num_oscils >= 1)
        oscil1.setFreq(key1_pitch);
        #endif
        #if (num_oscils >= 2)
        oscil2.setFreq(key2_pitch);
        #endif
        #if (num_oscils >= 3)
        oscil3.setFreq(key3_pitch);
        #endif
        #if (num_oscils >= 4)
        oscil4.setFreq(key4_pitch);
        #endif
        #if (num_oscils >= 5)
        oscil5.setFreq(key5_pitch);
        #endif
    #endif
    
    // Serial
    // #if ((num_oscils < number_of_keys) && (num_oscils > 1))
        // Serial.println("There are fewer oscillators than keys, and there is more than one oscillator.");
    // #endif
    
    #if ((num_oscils < number_of_keys) && (num_oscils > 1))
        if ((key1_is_pressed == true) && (key1_was_pressed == false) && (num_oscils_avail > 0)) {
            #if (num_oscils >= 5)
                if (oscil5_used_by == 0) {
                    oscil5.setFreq(key1_pitch);
                    oscil5_used_by = 1;
                } else
            #endif
            #if (num_oscils >= 4)
                if (oscil4_used_by == 0) {
                    oscil4.setFreq(key1_pitch);
                    oscil4_used_by = 1;
                } else
            #endif
            #if (num_oscils >= 3)
                if (oscil3_used_by == 0) {
                    oscil3.setFreq(key1_pitch);
                    oscil3_used_by = 1;
                } else
            #endif
                if (oscil2_used_by == 0) {
                    oscil2.setFreq(key1_pitch);
                    oscil2_used_by = 1;
                    
                    // Serial
                    // Serial.println("oscil2 used by key1");
                } else if (oscil1_used_by == 0) {
                    oscil1.setFreq(key1_pitch);
                    oscil1_used_by = 1;
                } else {
                    return; // If oscillators were available but none could be assigned, something went wrong
                }
            key1_was_pressed = true;
            number_keys_pressed += 1;
            key1_has_oscil = true;
            num_oscils_avail -= 1;
        } else if ((key1_is_pressed == false) && (key1_was_pressed == true) && (key1_has_oscil == true)) {
            // Serial
            // Serial.println("key1_was_pressed == false and key1_has_oscil == true");
            
            key1_was_pressed = false;
            number_keys_pressed -= 1;
            num_oscils_avail += 1;
            key1_has_oscil = false;
            #if (num_oscils >= 5)
                if (oscil5_used_by == 1) {
                    oscil5_used_by = 0;
                } else
            #endif
            #if (num_oscils >= 4)
                if (oscil4_used_by == 1) {
                    oscil4_used_by = 0;
                } else
            #endif
            #if (num_oscils >= 3)
                if (oscil3_used_by == 1) {
                    oscil3_used_by = 0;
                } else
            #endif
            #if (num_oscils >= 2)
                if (oscil2_used_by == 1) {
                    oscil2_used_by = 0;
                } else if (oscil1_used_by == 1) {
                    oscil1_used_by = 0;
                }
            #endif
        }
        if ((key2_is_pressed == true) && (key2_was_pressed == false) && (num_oscils_avail > 0)) {
            // Serial
            // Serial.println("(key2_is_pressed == true) && (key2_was_pressed == false) && (num_oscils_avail > 0)");
            
            #if (num_oscils >= 5)
                if (oscil5_used_by == 0) {
                    oscil5.setFreq(key2_pitch);
                    oscil5_used_by = 2;
                } else
            #endif
            #if (num_oscils >= 4)
                if (oscil4_used_by == 0) {
                    oscil4.setFreq(key2_pitch);
                    oscil4_used_by = 2;
                } else
            #endif
            #if (num_oscils >= 3)
                if (oscil3_used_by == 0) {
                    oscil3.setFreq(key2_pitch);
                    oscil3_used_by = 2;
                } else
            #endif
                if (oscil2_used_by == 0) {
                    oscil2.setFreq(key2_pitch);
                    oscil2_used_by = 2;
                } else if (oscil1_used_by == 0) {
                    oscil1.setFreq(key2_pitch);
                    oscil1_used_by = 2;
                } else {
                    return; // If oscillators were available but none could be assigned, something went wrong
                }
            key2_was_pressed = true;
            number_keys_pressed += 1;
            num_oscils_avail -= 1;
            key2_has_oscil = true;
        } else if ((key2_is_pressed == false) && (key2_was_pressed == true) && (key2_has_oscil == true)) {
            key2_was_pressed = false;
            number_keys_pressed -= 1;
            num_oscils_avail += 1;
            key2_has_oscil = false;
            #if (num_oscils >= 5)
                if (oscil5_used_by == 2) {
                    oscil5_used_by = 0;
                } else
            #endif
            #if (num_oscils >= 4)
                if (oscil4_used_by == 2) {
                    oscil4_used_by = 0;
                } else
            #endif
            #if (num_oscils >= 3)
                if (oscil3_used_by == 2) {
                    oscil3_used_by = 0;
                } else
            #endif
            #if (num_oscils >= 2)
                if (oscil2_used_by == 2) {
                    oscil2_used_by = 0;
                } else if (oscil1_used_by == 2) {
                    oscil1_used_by = 0;
                }
            #endif
        }
        if ((key3_is_pressed == true) && (key3_was_pressed == false) && (num_oscils_avail > 0)) {
            #if (num_oscils >= 5)
                if (oscil5_used_by == 0) {
                    oscil5.setFreq(key3_pitch);
                    oscil5_used_by = 3;
                } else
            #endif
            #if (num_oscils >= 4)
                if (oscil4_used_by == 0) {
                    oscil4.setFreq(key3_pitch);
                    oscil4_used_by = 3;
                } else
            #endif
            #if (num_oscils >= 3)
                if (oscil3_used_by == 0) {
                    oscil3.setFreq(key3_pitch);
                    oscil3_used_by = 3;
                } else
            #endif
                if (oscil2_used_by == 0) {
                    oscil2.setFreq(key3_pitch);
                    oscil2_used_by = 3;
                } else if (oscil1_used_by == 0) {
                    oscil1.setFreq(key3_pitch);
                    oscil1_used_by = 3;
                } else {
                    return; // If oscillators were available but none could be assigned, something went wrong
                }
            key3_was_pressed = true;
            number_keys_pressed += 1;
            num_oscils_avail -= 1;
            key3_has_oscil = true;
        } else if ((key3_is_pressed == false) && (key3_was_pressed == true) && (key3_has_oscil == true)) {
            key3_was_pressed = false;
            number_keys_pressed -= 1;
            num_oscils_avail += 1;
            key3_has_oscil = false;
            #if (num_oscils >= 5)
                if (oscil5_used_by == 3) {
                    oscil5_used_by = 0;
                } else
            #endif
            #if (num_oscils >= 4)
                if (oscil4_used_by == 3) {
                    oscil4_used_by = 0;
                } else
            #endif
            #if (num_oscils >= 3)
                if (oscil3_used_by == 3) {
                    oscil3_used_by = 0;
                } else
            #endif
            #if (num_oscils >= 2)
                if (oscil2_used_by == 3) {
                    oscil2_used_by = 0;
                } else if (oscil1_used_by == 3) {
                    oscil1_used_by = 0;
                }
            #endif
        }
        if ((key4_is_pressed == true) && (key4_was_pressed == false) && (num_oscils_avail > 0)) {
            #if (num_oscils >= 5)
                if (oscil5_used_by == 0) {
                    oscil5.setFreq(key4_pitch);
                    oscil5_used_by = 4;
                } else
            #endif
            #if (num_oscils >= 4)
                if (oscil4_used_by == 0) {
                    oscil4.setFreq(key4_pitch);
                    oscil4_used_by = 4;
                } else
            #endif
            #if (num_oscils >= 3)
                if (oscil3_used_by == 0) {
                    oscil3.setFreq(key4_pitch);
                    oscil3_used_by = 4;
                } else
            #endif
                if (oscil2_used_by == 0) {
                    oscil2.setFreq(key4_pitch);
                    oscil2_used_by = 4;
                } else if (oscil1_used_by == 0) {
                    oscil1.setFreq(key4_pitch);
                    oscil1_used_by = 4;
                } else {
                    return; // If oscillators were available but none could be assigned, something went wrong
                }
            key4_was_pressed = true;
            number_keys_pressed += 1;
            num_oscils_avail -= 1;
            key4_has_oscil = true;
        } else if ((key4_is_pressed == false) && (key4_was_pressed == true) && (key4_has_oscil == true)) {
            key4_was_pressed = false;
            number_keys_pressed -= 1;
            num_oscils_avail += 1;
            key4_has_oscil = false;
            #if (num_oscils >= 5)
                if (oscil5_used_by == 4) {
                    oscil5_used_by = 0;
                } else
            #endif
            #if (num_oscils >= 4)
                if (oscil4_used_by == 4) {
                    oscil4_used_by = 0;
                } else
            #endif
            #if (num_oscils >= 3)
                if (oscil3_used_by == 4) {
                    oscil3_used_by = 0;
                } else
            #endif
            #if (num_oscils >= 2)
                if (oscil2_used_by == 4) {
                    oscil2_used_by = 0;
                } else if (oscil1_used_by == 4) {
                    oscil1_used_by = 0;
                }
            #endif
        }
        if ((key5_is_pressed == true) && (key5_was_pressed == false) && (num_oscils_avail > 0)) {
            #if (num_oscils >= 5)
                if (oscil5_used_by == 0) {
                    oscil5.setFreq(key5_pitch);
                    oscil5_used_by = 5;
                } else
            #endif
            #if (num_oscils >= 4)
                if (oscil4_used_by == 0) {
                    oscil4.setFreq(key5_pitch);
                    oscil4_used_by = 5;
                } else
            #endif
            #if (num_oscils >= 3)
                if (oscil3_used_by == 0) {
                    oscil3.setFreq(key5_pitch);
                    oscil3_used_by = 5;
                } else
            #endif
                if (oscil2_used_by == 0) {
                    oscil2.setFreq(key5_pitch);
                    oscil2_used_by = 5;
                } else if (oscil1_used_by == 0) {
                    oscil1.setFreq(key5_pitch);
                    oscil1_used_by = 5;
                } else {
                    return; // If oscillators were available but none could be assigned, something went wrong
                }
            key5_was_pressed = true;
            number_keys_pressed += 1;
            num_oscils_avail -= 1;
            key5_has_oscil = true;
        } else if ((key5_is_pressed == false) && (key5_was_pressed == true) && (key5_has_oscil == true)) {
            key5_was_pressed = false;
            number_keys_pressed -= 1;
            num_oscils_avail += 1;
            key5_has_oscil = false;
            #if (num_oscils >= 5)
                if (oscil5_used_by == 5) {
                    oscil5_used_by = 0;
                } else
            #endif
            #if (num_oscils >= 4)
                if (oscil4_used_by == 5) {
                    oscil4_used_by = 0;
                } else
            #endif
            #if (num_oscils >= 3)
                if (oscil3_used_by == 5) {
                    oscil3_used_by = 0;
                } else
            #endif
            #if (num_oscils >= 2)
                if (oscil2_used_by == 5) {
                    oscil2_used_by = 0;
                } else if (oscil1_used_by == 5) {
                    oscil1_used_by = 0;
                }
            #endif
        }
    #endif
    #if (num_oscils == 1)
        if (key1_is_pressed == true) {
            oscil1.setFreq(key1_pitch);
            oscil1_used_by = 1;
        } else if (key2_is_pressed == true) {
            oscil1.setFreq(key2_pitch);
            oscil1_used_by = 2;
            
            // Serial
            // Serial.println("oscil1_used_by 2");
        } else if (key3_is_pressed == true) {
            oscil1.setFreq(key3_pitch);
            oscil1_used_by = 3;
        } else if (key4_is_pressed == true) {
            oscil1.setFreq(key4_pitch);
            oscil1_used_by = 4;
        } else if (key5_is_pressed == true) {
            oscil1.setFreq(key5_pitch);
            oscil1_used_by = 5;
        }
    #endif
}

void setup() {
    // Serial
    Serial.begin(9600,SERIAL_8N1);
    delay(100);
    Serial.println("Serial works!");
    delay(10);

    // CapSense
    cs_key1.CS_Timeout_Millis = timeout_cycles;
    cs_key2.CS_Timeout_Millis = timeout_cycles;
    cs_key3.CS_Timeout_Millis = timeout_cycles;
    cs_key4.CS_Timeout_Millis = timeout_cycles;
    cs_key5.CS_Timeout_Millis = timeout_cycles;
    cs_mode.CS_Timeout_Millis = timeout_cycles;
    cs_up.CS_Timeout_Millis = timeout_cycles;
    cs_down.CS_Timeout_Millis = timeout_cycles;
    cs_key1.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_key2.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_key3.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_key4.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_key5.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_mode.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_up.CS_AutocaL_Millis = 0xFFFFFFFF;
    cs_down.CS_AutocaL_Millis = 0xFFFFFFFF;
    
    #if (num_oscils >= 1)
    oscil1.setFreq(fundamental_frequency);
    #endif
    #if (num_oscils >= 2)
    oscil2.setFreq(fundamental_frequency);
    #endif
    #if (num_oscils >= 3)
    oscil3.setFreq(fundamental_frequency);
    #endif
    // Extra oscillators if there's enough CPU time for them
    #if (num_oscils >= 4)
    oscil4.setFreq(fundamental_frequency);
    #endif
    #if (num_oscils >= 5)
    oscil5.setFreq(fundamental_frequency);
    #endif
}

void updateControl() {
    check_button_states();
    #if (num_oscils > 1)
    check_inputs();
    #endif
    set_notes();
    
    // Serial
    Serial.print("key1_was_pressed: ");
    Serial.print("\t");
    if (key1_was_pressed == true) {
        Serial.println("true");
    } else {
        Serial.println("false");
    }
    Serial.print("key3_was_pressed: ");
    Serial.print("\t");
    if (key3_was_pressed == true) {
        Serial.println("true");
    } else {
        Serial.println("false");
    }
}

int updateAudio() {
    // long total_audio = 0;
    int16_t total_audio = 0;
    uint8_t total_oscils = 0;
    
    if (oscil1_used_by > 0) {
        total_audio += oscil1.next();
        total_oscils += 1;
        
        // Serial
        // Serial.print(total_audio);
        // Serial.print("\t");
        // Serial.print(total_oscils);
        // Serial.print("\t");
    }
    #if (num_oscils >= 2)
    if (oscil2_used_by > 0) {
        total_audio += oscil2.next();
        total_oscils += 1;
        
        // Serial
        // Serial.print(total_audio);
        // tab;
        // Serial.print(total_oscils);
        // nl;
    }
    #endif
    #if (num_oscils >= 3)
    if (oscil3_used_by > 0) {
        total_audio += oscil3.next();
        total_oscils += 1;
        
        // Serial
        Serial.print(total_audio);
        tab;
        Serial.print(total_oscils);
        nl;
    }
    #endif
    #if (num_oscils >= 4)
    if (oscil4_used_by > 0) {
        total_audio += oscil4.next();
        total_oscils += 1;
    }
    #endif
    #if (num_oscils >= 5)
    if (oscil5_used_by > 0) {
        total_audio += oscil5.next();
        total_oscils += 1;
    }
    #endif
    
    if (total_oscils == 0) {
        return 0;
    } else if (total_oscils == 1) {
        // Serial
        // Serial.print(total_audio);
        // Serial.print("\t");
        // Serial.print(total_oscils);
        // Serial.print("\t");
        // Serial.println(volume);
        
        return (total_audio*volume) >> 4;
    }
    #if (num_oscils >= 2)
    else if (total_oscils == 2) {
        return (total_audio*volume) >> 5;
    }
    #endif
    #if (num_oscils >= 3)
    else if (total_oscils == 3) {
        int16_t val = total_audio*volume;
        // if (val > 0) {
            // val = ((int16_t) divu3((unsigned int) val));
        // } else if (val < 0) {
            // val = (0 - val); // Make it positive
            // val = ((int16_t) divu3((unsigned int) val)); // Divide it by three
            // val = (0 - val); // Turn the result back to negative
        // }
        // return val >> 4;
        
        return ((val < 0) ? (0 - ((int16_t) divu3((unsigned int) (0 - val)))) : ((int16_t) divu3((unsigned int) val))) >> 4;
        // return ((total_audio*volume)/3) >> 4;
    }
    #endif
    #if (num_oscils >= 4)
    else if (total_oscils == 4) {
        return (total_audio*volume) >> 6;
    }
    #endif
    #if (num_oscils >=5)
    else if (total_oscils == 5) {
        return ((total_audio*volume)/5) >> 4;
    }
    #endif
    
    // return (((oscil1.next())*volume) >> 8);
    
    // return oscil1.next();
}

void loop() {
    updateControl();
    updateAudio();
    
    // Timing testing
    // unsigned long start_time = micros();
    // check_button_states();
    // check_inputs();
    // set_notes();
    // updateControl();
    // updateAudio();
    // unsigned long end_time = micros();
    
    // Serial
    // Serial.println(end_time - start_time);
    Serial.println("Finished one loop");
    
    // Pause
    // while (true) {continue;}
    
    delay(1000);
}